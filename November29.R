library(TSA)
data("hare")
plot(hare,type='o')
plot(y=hare,x=zlag(hare))
acf(hare) #opr MA 
pacf(hare) #opr reda na awtoregresiq

BoxCox.ar(hare)
d=BoxCox.ar(hare)
d
#=>
plot(hare^0.5)
acf(hare^0.5)
pacf(hare^0.5)
#ne vurshi rabota mai 

#ocenqwame
model1=arima(sqrt(hare),order=c(3,0,0))
model1 #aic = 101.08, koef znachimi li sa - ar1 da na granicata, ar2 ne e, ar3 ne e znachim
#(delim na 10 zashtoto iskame edin porqdyk po-malko) 
acf(residuals(model1)) #dobre e, wuoreki che imahme sezonnost i dwa neznachimi koeficenta
pacf(residuals(model1)) #obiknoveno se durji po dobre ot acf 
#po-hubawo e da se prawqt standarizirani ostatuci (tezi sega sa mnogo malki)
acf(rstandard(model1))

plot(residuals(model1))
plot(rstandard(model1))

plot(model1) #prognozata ili:
plot(model1,n.ahead = 50)#doveritelniq int se razshirqwa, funkciq na wremeto e (za 50 godini napred e)

#ar2 e neznachim i iskame da go mahnem:
model2=arima(sqrt(hare),order=c(3,0,0),fixed = c(NA,0,NA,NA))
model2 #aic = 99.69

#srawnqwane na konkurirashti modeli w time series se naricha

acf(rstandard(model2))
pacf(rstandard(model2))

#kolkoto po-malak aic tolkowa po-dobre 

model3=arima(sqrt(hare),order=c(2,0,0)) #toest mahame ar3 koeto e fi3
model3 #pak e neznachim ar2, aic e 102,koeto e po-zle
acf(residuals(model3))
pacf(residuals(model3))

model4=arima(sqrt(hare),order=c(1,0,0)) #AR(1)
model4
acf(residuals(model4))
#ne e dobre tozi model 

qqnorm(rstandard(model2))
qqline(rstandard(model2))
shapiro.test(rstandard(model2)) #p-value = 0.03257

qqnorm(runif(n=100))
qqline(runif(n=100))

qqnorm(rcauchy(n=100)) #primer za tejki opasjhki, gledame na kude e zawurtqna
qqline(rcauchy(n=100))

ks.test(x=rstandard(model2),y='pnorm') #ne se hareswa mnogo, za malko nabludeniq ne e dobre 
#zaradi stpawolidnostta za koqto trqbwat mnogo danni  

#dali shte bade po-normalno ako wzemem nekorenuwanite danni 
model5=arima(hare,order = c(3,0,0),fixed = c(NA,0,NA,NA))
model5 #aic = 257.44 ne znachi che e losho 
acf(rstandard(model5))
pacf(rstandard(model5))
qqnorm(rstandard(model5))
qqline(rstandard(model5))
shapiro.test(rstandard(model5)) #p-value = 0.6304, koeto e dobre

detectAO(model2,robust = FALSE)
detectIO(model2)

Box.test(residuals(model2),lag = 5,type=c("Ljung-Box"),fitdf = 0)
tsdiag(model2,gof=15)

