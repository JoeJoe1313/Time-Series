library("tseries")
library("TSA")
library('forecast')
library('stats')

set.seed(3)
model=arima.sim(list(order=c(0,1,3), ma=c(0.05, -0.175, 0.025)), sd=0.5, n=1000)

plot(model,type='o')
acf(model)
pacf(model)
eacf(model) 
adf.test(model)

plot(diff(model),type='o')
acf(diff(model)) 
pacf(diff(model)) 
eacf(diff(model)) 
adf.test(diff(model))

model1=arima(model, order=c(0,1,2))
model1
plot(model1, n.ahead=10)

model2=arima(model, order=c(2,1,0))
model2

model3=arima(model, order=c(1,1,2))
model3

model4=arima(model, order=c(0,1,3))
model4

model5=arima(model, order=c(0,1,1))
model5

y_model=Arima(model,order=c(0,1,2))
y_model
auto.arima(model)

plot(residuals(y_model),type='o')
checkresiduals(y_model)
acf(residuals(y_model))
pacf(residuals(y_model))
qqnorm(residuals(y_model))
qqline(residuals(y_model), col='red', lwd=2)
shapiro.test(residuals(y_model))

plot(forecast(y_model,h=10))
